<?php
/**
 * @file
 * vactory_wysiwyg_templates.features.wysiwyg_template.inc
 */

/**
 * Implements hook_wysiwyg_template_default_templates().
 */
function vactory_wysiwyg_templates_wysiwyg_template_default_templates() {
  $templates = array();
  $templates['bloquotes'] = array(
    'title' => 'Bloquotes',
    'description' => 'Bloquotes',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row"><div class="col-md-12"><blockquote class="quote"><p class="text-justify">Le <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre ...</p><footer class="blockquote-footer">Author</footer></blockquote></div></div>',
    'format' => 'full_html',
    'name' => 'bloquotes',
    'content_types' => array(),
  );
  $templates['slider'] = array(
    'title' => 'Slider',
    'description' => 'A Bootstrap Slider',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="carousel slide" data-ride="carousel" id="myCarousel"><!-- Indicators --><ol class="carousel-indicators"><li class="active" data-slide-to="0" data-target="#myCarousel">&nbsp;</li><li data-slide-to="1" data-target="#myCarousel">&nbsp;</li><li data-slide-to="2" data-target="#myCarousel">&nbsp;</li></ol><!-- Wrapper for slides --><div class="carousel-inner" role="listbox"><div class="item active">[[{"fid":"1","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":false,"field_file_image_title_text[und][0][value]":false},"type":"media","attributes":{"style":"width: 100%;","class":"media-element file-default"}}]]<div class="carousel-caption"><p>Carousel 1</p></div></div><div class="item">[[{"fid":"1","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":false,"field_file_image_title_text[und][0][value]":false},"type":"media","attributes":{"style":"width: 100%;","class":"media-element file-default"}}]]<div class="carousel-caption"><p>Carousel 2</p></div></div><div class="item">[[{"fid":"1","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":false,"field_file_image_title_text[und][0][value]":false},"type":"media","attributes":{"style":"width: 100%;","class":"media-element file-default"}}]]<div class="carousel-caption"><p>Carousel 3</p></div></div></div><!-- Left and right controls --><a class="left carousel-control" data-slide="prev" href="#myCarousel" role="button"><span class="sr-only">Previous</span> </a> <a class="right carousel-control" data-slide="next" href="#myCarousel" role="button"> <span class="sr-only">Next</span> </a></div><p>&nbsp;</p>',
    'format' => 'full_html',
    'name' => 'slider',
    'content_types' => array(),
  );
  $templates['text_one_column'] = array(
    'title' => 'Text one column',
    'description' => 'Text on one Column (Justified)',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row"><div class="col-md-12"><p class="text-justify">Le <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p></div></div>',
    'format' => 'full_html',
    'name' => 'text_one_column',
    'content_types' => array(),
  );
  $templates['text_three_column'] = array(
    'title' => 'Text three column',
    'description' => 'Text on three Column (Justified)',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row">
  <div class="col-md-4">
    <p class="text-justify">Le
      <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le
      Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble
      des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,
      mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé
      dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
      par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
  </div>
  <div class="col-md-4">
    <p class="text-justify">Le
      <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le
      Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble
      des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,
      mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé
      dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
      par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
  </div>
  <div class="col-md-4">
    <p class="text-justify">Le
      <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le
      Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble
      des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,
      mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé
      dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
      par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
  </div>
</div>',
    'format' => 'full_html',
    'name' => 'text_three_column',
    'content_types' => array(),
  );
  $templates['text_two_column_'] = array(
    'title' => 'Text two column ',
    'description' => 'Text on two Column (Justified)',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row">
  <div class="col-md-6">
    <p class="text-justify">Le
      <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le
      Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble
      des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,
      mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé
      dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
      par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
  </div>
  <div class="col-md-6">
    <p class="text-justify">Le
      <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le
      Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble
      des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,
      mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé
      dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
      par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
  </div>
</div>',
    'format' => 'full_html',
    'name' => 'text_two_column_',
    'content_types' => array(),
  );
  $templates['text_with_button'] = array(
    'title' => 'Text with button',
    'description' => 'Text on one Column (Justified) with button',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row">
  <div class="col-md-12">
    <p class="text-justify">Le
      <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le
      Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble
      des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles,
      mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé
      dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
      par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
    <br/>
    <a href="#" class="btn btn-default btn-lg">view more</a>
  </div>
</div>',
    'format' => 'full_html',
    'name' => 'text_with_button',
    'content_types' => array(),
  );
  $templates['text_with_image_right_'] = array(
    'title' => 'Text with image (right)',
    'description' => 'Text with an image at right side',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row"><div class="col-md-6"><p class="text-justify">Le <strong>Lorem Ipsum</strong>est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p></div><div class="col-md-6">[[{"fid":"1","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":false,"field_file_image_title_text[und][0][value]":false},"type":"media","attributes":{"height":"251","width":"500","class":"media-element img-thumbnail file-default"}}]]</div></div>',
    'format' => 'full_html',
    'name' => 'text_with_image_right_',
    'content_types' => array(),
  );
  $templates['two_block_image_text_and_button'] = array(
    'title' => 'Two Block : Image, text and button',
    'description' => 'Two Blocks : Each block contains an image, text and CTA',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="row">
  <div class="col-md-6 text-justify">
[[{"fid":"1","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":false,"field_file_image_title_text[und][0][value]":false},"type":"media","attributes":{"height":"251","width":"500","class":"media-element img-responsive file-default"}}]]
    <br/>
    <p>Le
      <b>Lorem Ipsum</b>est simplement du faux texte employé dans la composition et la mise en page avant impression.</p>
    <br/>
    <a href="#" class="btn btn-default">view more</a>
  </div>
  <div class="col-md-6 text-justify">
[[{"fid":"1","view_mode":"default","fields":{"format":"default","field_file_image_alt_text[und][0][value]":false,"field_file_image_title_text[und][0][value]":false},"type":"media","attributes":{"height":"251","width":"500","class":"media-element img-responsive file-default"}}]]
    <br/>
    <p>Le
      <b>Lorem Ipsum</b>est simplement du faux texte employé dans la composition et la mise en page avant impression.</p>
    <br/>
    <a href="#" class="btn btn-default">view more</a>
  </div>
</div>',
    'format' => 'full_html',
    'name' => 'two_block_image_text_and_button',
    'content_types' => array(),
  );
  return $templates;
}
