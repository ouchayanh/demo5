<?php
/**
 * @file
 * voidforum.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function voidforum_default_rules_configuration() {
  $items = array();
  $items['rules_comments_notifications'] = entity_import('rules_config', '{ "rules_comments_notifications" : {
      "LABEL" : "comments notifications save",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "comment" ],
      "ON" : { "comment_insert" : [] },
      "IF" : [ { "data_is" : { "data" : [ "comment:status" ], "value" : "0" } } ],
      "DO" : [
        { "mail" : {
            "to" : "[comment:mail]",
            "subject" : "[comment:title]",
            "message" : "[comment:body]\\r\\nVotre commentaire est en cours d\\u0027approbation",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_comments_notifications_publier'] = entity_import('rules_config', '{ "rules_comments_notifications_publier" : {
      "LABEL" : "comments notifications publier",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "comment" ],
      "ON" : { "comment_update--comment_node_forum" : { "bundle" : "comment_node_forum" } },
      "IF" : [ { "data_is" : { "data" : [ "comment:status" ], "value" : "1" } } ],
      "DO" : [
        { "mail" : {
            "to" : "[comment:mail]",
            "subject" : "[comment:title]",
            "message" : "[comment:body] \\r\\nvotre commentaire est valid\\u00e9",
            "from" : "[site:mail]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
