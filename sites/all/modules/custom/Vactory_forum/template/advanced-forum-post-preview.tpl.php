<?php

/**
 * @file
 * Theme implementation: Template for each forum post whether node or comment.
 *
 * All variables available in node.tpl.php and comment.tpl.php for your theme
 * are available here. In addition, Advanced Forum makes available the following
 * variables:
 *
 * - $top_post: TRUE if we are formatting the main post (ie, not a comment)
 * - $reply_link: Text link / button to reply to topic.
 * - $total_posts: Number of posts in topic (not counting first post).
 * - $new_posts: Number of new posts in topic, and link to first new.
 * - $links_array: Unformatted array of links.
 * - $account: User object of the post author.
 * - $name: User name of post author.
 * - $author_pane: Entire contents of the Author Pane template.
 */
?>
<?php if ($top_post): ?>
    <?php print $topic_header ?>
<?php endif; ?>

<div id="<?php print $top_post ? "node" : "comment-forum"; ?>-<?php print $post_id; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
    <div class="forum-post-wrapper">
        <?php if (!empty($in_reply_to)): ?>
            <span class="forum-in-reply-to"><?php print $in_reply_to; ?></span>
        <?php endif; ?>

        <?php if (!$node->status): ?>
            <span class="unpublished-post-note"><?php print t("Unpublished post") ?></span>
        <?php endif; ?>
        <div class="forum-post-panel-main clearfix" style="margin-left: unset;">
            <?php if (!empty($title) && $top_post): ?>
                <div class="forum-post-title">
                    <?php print $title ?>
                </div>
            <?php endif; ?>
            <?php
            if ($top_post) {
                if($comment==2){
                    print t("Ouvert");
                }
                if($comment==1){
                    print t("Fermé");
                }
            }
            ?>
            <div class="forum-post-content">
                <?php
                // @codingStandardsIgnoreStart
                // We hide the comments and links now so that we can render them later.
                hide($content['taxonomy_forums']);
                hide($content['comments']);
                hide($content['links']);
                if (!$top_post){
                    print $date;
                    $user = user_load($comment->uid);
                    $link = url('user/' . $user->uid);
                    ?>
                    <?php print $user->name; ?>
                    <a href="<?php print $link ?>">Contactez-moi</a>
                    <div class="nom-prenom">
                        <?php //print $user->field_pr_nom['und']['0']['value']; ?>
                    </div>
                    <?php
                    if (!empty($user->picture)) {
                        print theme('image_style', array('path' => $user->picture->uri, 'style_name' => 'thumbnail'));
                    }
                    //else {
                    //$custom_default_image_path = 'public://default_image.png';
                    //print theme('image_style', array('path' => variable_get('user_picture_default', $custom_default_image_path), 'style_name' => 'thumbnail'));
                    //}
                    hide($content['body']);
                    print render($content);
                }else{
                    hide($content['field_editeur_forum']);
                    hide($content['field_image_forum']);
                    print render($content);
                    $editor = user_load($content['field_editeur_forum']['#items'][0]['uid']);
                    ?>
                    <?php print $editor->name; ?>
                    <div class="nom-prenom">
                        <?php //print $editor->field_pr_nom['und']['0']['value']; ?>
                    </div>
                    <div class="activite">
                        <?php print $editor->field_activite['und']['0']['value']; ?>
                    </div>
                    <?php
                    if (!empty($editor->picture)) {
                        print theme('image_style', array('path' => $editor->picture->uri, 'style_name' => 'thumbnail'));
                    }
                    //else {
                    //$custom_default_image_path = 'public://default_image.png';
                    //print theme('image_style', array('path' => variable_get('user_picture_default', $custom_default_image_path), 'style_name' => 'thumbnail'));
                    //}
                    $link = url('user/' . $editor->uid);
                    ?>
                    <a href="<?php print $link ?>">Contactez-moi</a>
                    <?php
                }
                ?>
            </div>

        </div>
    </div>
    <?php /* End of post wrapper div. */ ?>

    <div class="forum-post-footer clearfix">
        <div class="forum-post-links">
            <?php print render($content['links']); ?>
        </div>
    </div>
    <?php /* End of footer div. */ ?>
</div>
<?php /* End of main wrapping div. */ ?>

