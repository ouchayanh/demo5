<div class="forum">
    <div class="image-forum">
            <?php print $fields["field_forum_image"]->content; ?>
    </div>
    <div class="discussion-list">
        <div class="titre">
            <h2><?php print $fields["title"]->content; ?></h2>
        </div>
        <div class="description">
            <p><?php print $fields["body"]->content; ?></p>
        </div>
        <div class="comment-number">
            <p><i class="fa fa-comments-o" aria-hidden="true"></i> <?php print $fields["comment_count"]->content; ?></p>
        </div>
    </div>
</div>