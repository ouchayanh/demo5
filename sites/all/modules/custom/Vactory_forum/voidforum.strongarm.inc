<?php
/**
 * @file
 * voidforum.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function voidforum_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'advanced_forum_forum_image_field';
  $strongarm->value = 'field_forum_image';
  $export['advanced_forum_forum_image_field'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'advanced_forum_forum_image_preset';
  $strongarm->value = 'thumbnail';
  $export['advanced_forum_forum_image_preset'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'advanced_forum_forum_user_term_fields';
  $strongarm->value = 1;
  $export['advanced_forum_forum_user_term_fields'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'advanced_forum_user_picture_preset';
  $strongarm->value = 'thumbnail';
  $export['advanced_forum_user_picture_preset'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeviewcount_node_types';
  $strongarm->value = array(
    'forum' => 'forum',
  );
  $export['nodeviewcount_node_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeviewcount_user_roles';
  $strongarm->value = array(
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
  );
  $export['nodeviewcount_user_roles'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeviewcount_way_counting';
  $strongarm->value = 0;
  $export['nodeviewcount_way_counting'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'user_pictures';
  $strongarm->value = '1';
  $export['user_pictures'] = $strongarm;

  return $export;
}
