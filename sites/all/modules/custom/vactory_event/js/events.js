/**
 * @file
 * Js file for events.
 */
(function ($) {
    $(document).ready(function () {
        if($('.node-type-event').length) {
            var text = $('.node-type-event  .page-header').text();
            $('.node-type-event .node-event .content .field-name-field-vactory-image-cover').after('<div class="field-name-field-title">'+text+'</div>');
        }

        if($('.page-events').length) {
            $('#content').removeClass('container');
            $('.view-content , .view-filters > form').addClass('container');
            $('.region-content  >.view-vactory-events-push ').next().addClass('container');
        }
        $('.views-exposed-widget select.form-select').select2();

    });


})(jQuery);