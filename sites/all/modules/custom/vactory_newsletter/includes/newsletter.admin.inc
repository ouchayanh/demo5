<?php



function vactory_newsletter_configure($form, &$form_state) {
    $lists = db_query('SELECT nlid, title FROM {newsletter_list}')->fetchAllKeyed();
    $personal_info_form = variable_get('newsletter_personal_info_form');
    $personal_info_options = array(
        'hidden' => t('hide'),
        'show' => t('show'),
        'require' => t('require'),
    );

    $form['test'] = array(
        '#attributes' => array('class' => array('container-inline')),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Send a test Email'),
    );
    $form['test']['newsletter_test_to'] = array(
        '#type' => 'textfield',
        '#title' => t('To'),
        '#default_value' => variable_get('newsletter_test_to', variable_get('site_mail')),
    );
    $form['test']['send_test'] = array(
        '#type' => 'submit',
        '#value' => t('Send'),
    );


    $form['settings'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#title' => t('General settings'),
    );

    $form['settings']['newsletter_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Titre'),
        '#default_value' => variable_get('newsletter_title')
    );


    $form['settings']['newsletter_subtitle'] = array(
        '#type' => 'textarea',
        '#title' => t('Contenu '),
        '#format' => 'full_html',
        '#default_value' => variable_get('newsletter_subtitle')
    );

    $form['settings']['newsletter_type'] = array(
        '#type' => 'select',
        '#tree' => TRUE,
        '#title' => t('Type Affichage'),
        '#options' => array(
            'asWidget' => t('Fixed Bottom'),
            'asFixedButton' => t('Fixed Right'),
            'asSidebarItem' => t('Sidebar Item'),
            'asPrefooter' => t('Pre Footer'),
            'asSinglePage' => t('As Single page')
        ),
        '#default_value' => variable_get('newsletter_type')
    );
    $form['settings']['newsletter_from'] = array(
        '#type' => 'textfield',
        '#title' => t('From'),
        '#size' => 60,
        '#maxlength' => 128,
        '#default_value' => variable_get('newsletter_from', variable_get('site_mail')),
        '#description' => t('Suggested format: User &lt;user@example.com&gt;'),
    );
    $form['settings']['newsletter_format'] = array(
        '#type' => 'select',
        '#title' => t('Format'),
        '#options' => array(
            'plain' => t('Plain Text'),
            'html' => t('HTML'),
        ),
        '#default_value' => variable_get('newsletter_format', 'html'),
    );
    $form['settings']['newsletter_track_open_rate'] = array(
        '#type' => 'checkbox',
        '#title' => t('Track open rate'),
        '#default_value' => variable_get('newsletter_track_open_rate', FALSE),
        '#description' => t('Enable it, only if you really want the email to be tracked,
      since the method used might cause some email clients to mark your emails as spam'),
    );
    $form['settings']['newsletter_cron_number'] = array(
        '#type' => 'textfield',
        '#title' => t('Number of mails to send per cron run'),
        '#default_value' => variable_get('newsletter_cron_number', 500),
        '#description' => t('The more newsletter lists the smaller this number should be.'),
    );
    $form['settings']['newsletter_node_limit'] = array(
        '#type' => 'textfield',
        '#title' => t('Number of nodes to send per newsletter'),
        '#default_value' => variable_get('newsletter_node_limit', 50),
        '#description' => t('The max number of nodes to send with a newsletter.'),
    );
    $form['settings']['newsletter_send_confirm'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send confirmation email'),
        '#default_value' => variable_get('newsletter_send_confirm'),
        '#description' => t('Attention: If you disable confirmation e-mails, users will be able to subscribe others without their permission.'),
    );
    $form['settings']['newsletter_send_welcome'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send welcome email'),
        '#default_value' => variable_get('newsletter_send_welcome'),
    );
    $form['settings']['newsletter_send_unsubscribe'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send unsubscribe email, after a subscriber decides to unsubscribe'),
        '#default_value' => variable_get('newsletter_send_unsubscribe'),
    );
    $form['settings']['newsletter_auto_subscribe_new_users'] = array(
        '#type' => 'select',
        '#title' => t('Automatically subscribe newly registered user to the selected lists:'),
        '#options' => array_map('check_plain', $lists),
        '#multiple' => TRUE,
        '#description' => t('Leave empty to bypass this feature'),
        '#default_value' => variable_get('newsletter_auto_subscribe_new_users', array()),
    );
    $form['settings']['newsletter_use_smtp'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable SMTP support'),
        '#default_value' => variable_get('newsletter_use_smtp', FALSE),
        '#description' => t('Requires <a href=@url>SMTP</a> module', array('@url' => 'http://drupal.org/project/smtp')),
    );
    $form['subscribe_form'] = array(
        '#type' => 'fieldset',
        '#title' => t('Subscribe form'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['subscribe_form']['newsletter_show_email'] = array(
        '#type' => 'checkbox',
        '#title' => t('Always show e-mail in the subscribe form.'),
        '#default_value' => variable_get('newsletter_show_email', TRUE),
    );
    $form['subscribe_form']['newsletter_show_email_in_block'] = array(
        '#type' => 'checkbox',
        '#title' => t('Always show e-mail in the subscribe form block.'),
        '#default_value' => variable_get('newsletter_show_email_in_block', FALSE),
    );
    $form['subscribe_form']['newsletter_always_show_subscribe_block'] = array(
        '#type' => 'checkbox',
        '#title' => t('Always show subscribe block,even if user is subscribed.'),
        '#default_value' => variable_get('newsletter_always_show_subscribe_block', FALSE),
    );
    $form['subscribe_form']['newsletter_personal_info_form'] = array(
        '#type' => 'fieldset',
        '#title' => t('Personal info form'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
    );
    $form['subscribe_form']['newsletter_personal_info_form']['firstname'] = array(
        '#attributes' => array('class' => array('container-inline')),
        '#type' => 'radios',
        '#title' => t('First Name (textfield)'),
        '#options' => $personal_info_options,
        '#default_value' => $personal_info_form['firstname'],
    );
    $form['subscribe_form']['newsletter_personal_info_form']['lastname'] = array(
        '#attributes' => array('class' => array('container-inline')),
        '#type' => 'radios',
        '#title' => t('Last Name (textfield)'),
        '#options' => $personal_info_options,
        '#default_value' => $personal_info_form['lastname'],
    );
    $form['subscribe_form']['newsletter_personal_info_form']['gender'] = array(
        '#attributes' => array('class' => array('container-inline')),
        '#type' => 'radios',
        '#title' => t('Gender (select)'),
        '#options' => $personal_info_options,
        '#default_value' => $personal_info_form['gender'],
    );
    $form['subscribe_form']['newsletter_personal_info_form']['receive_format'] = array(
        '#attributes' => array('class' => array('container-inline')),
        '#type' => 'radios',
        '#title' => t('Preferred Format (select)'),
        '#options' => $personal_info_options,
        '#default_value' => $personal_info_form['receive_format'],
    );
    $form = system_settings_form($form);

    $form['#submit'][] = 'vactory_newsletter_configure_submit';
    return $form;
}

function vactory_newsletter_configure_submit($form, &$form_state) {
    db_query("DELETE FROM {cache};");
    $default_value = variable_get('newsletter_type');
    if($default_value === 'asSinglePage') {
        $items = menu_load_links('main-menu');
        $isAlreadyLink = false;
        foreach($items as $item){
            if($item["link_path"] === 'newsletter/subscribe') {
                $isAlreadyLink = true;
            }
        }
        if (!$isAlreadyLink) {
            $item = array(
                'menu_name' => 'main-menu',
                'link_title' => 'Subscribe to newsletter',
                'link_path' => 'newsletter/subscribe',
                'customized' => 1,
            );
            $item['options']['attributes']['class'] = 'yop';

            menu_link_save($item);
        }

    }
    else {
        menu_link_delete(null , 'newsletter/subscribe');
    }
    menu_rebuild();


    if ($form_state['clicked_button']['#value'] == t('Send')) {

        $status = newsletter_create()->sendTest(variable_get('newsletter_test_to', variable_get('site_mail')));
        if ($status) {
            drupal_set_message(t('Test mail sent successfully!'), 'status');
        }
        else {
            drupal_set_message(t('Your test mail failed to be delivered.Please check your server logs for more information.'), 'error');
        }
    }
}

function newsletter_subscriber_list($form, &$form_state) {

    if (isset($form_state['storage']['confirm'])) {
        $form['operation'] = array(
            '#type' => 'hidden',
            '#value' => 'delete',
        );
        $form['subscriber'] = array(
            '#type' => 'hidden',
            '#value' => $form_state['values']['subscriber'],
        );
        $output = t('Are you sure you want to delete the following newsletter subscribers?');
        $output .= '<ul>';
        $subscribers = newsletter_subscriber_load($form_state['values']['subscriber']);
        foreach ($subscribers as $subscriber) {
            $output .= !empty($subscriber) ? '<li>' . check_plain($subscriber->email) . '</li>' : '';
        }
        $output .= '</ul>';
        $output .= t('This action cannot be undone.');
        return confirm_form($form, t('Delete the following?'), 'admin/config/media/newsletter/subscribers', filter_xss($output));
    }
    $form['options'] = array(
        '#type' => 'fieldset',
        '#title' => t('Options'),
        '#attributes' => array('class' => array('container-inline')),
    );

    $options['delete'] = t('Delete the selected subscribers');

    $form['options']['operation'] = array(
        '#type' => 'select',
        '#title' => t('Operation'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => 'delete',
    );
    $form['options']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
    );

    $header = array(
        'id' => array('data' => t('ID'), 'field' => 'nsid'),
        'email' => array('data' => t('Email'), 'field' => 'email'),
        'info' => array('data' => t('Name')),
        'taxonomy' => array('data' => t('Selected Choices')),
        'list_title' => array('data' => t('Subscribed to')),
        'created' => array('data' => t('Created'), 'field' => 'created', 'sort' => 'desc'),
        'confirmed' => array('data' => t('Confirmed')),
        'operations' => array('data' => t('Operations')),
    );

    $query = db_select('newsletter_subscriber', 'subscribers')->extend('PagerDefault')->extend('TableSort');
    $result = $query
        ->fields('subscribers')
        ->limit(50)
        ->orderByHeader($header)
        ->execute();

    $options = array();
    $destination = drupal_get_destination();
    foreach ($result as $row) {

        $info = array('data' => array());
        $info['data'][] = !empty($row->firstname) ? array('#markup' => t('Firstname:') . check_plain($row->firstname) . '<br />') : NULL;
        $info['data'][] = !empty($row->lastname) ? array('#markup' => t('Lastname:') . check_plain($row->lastname) . '<br />') : NULL;
        $info['data'][] = !empty($row->age_group) ? array('#markup' => t('Age group:') . check_plain($row->age_group) . '<br />') : NULL;
        $info['data'][] = !empty($row->job) ? array('#markup' => t('Job:') . check_plain($row->job) . '<br />') : NULL;
        $info['data'][] = !empty($row->gender) ? array('#markup' => t('Gender:') . check_plain($row->gender) . '<br />') : NULL;
        $info['data'][] = !empty($row->receive_format) ? array('#markup' => t('Preferred format:') . check_plain($row->receive_format) . '<br />') : NULL;
        $info['data'][] = !empty($row->language) ? array('#markup' => t('Language:') . check_plain($row->language) . '<br />') : NULL;

        $lists = db_query('SELECT title
      FROM {newsletter_list} list
      JOIN {field_data_field_newsletter_list} sub_index
      ON sub_index.field_newsletter_list_target_id = list.nlid
      WHERE sub_index.entity_id = :id',
            array(':id' => $row->nsid ))
            ->fetchCol();
        $lists = implode(', ', $lists);

        $options[$row->nsid] = array(
            'id' => (int) $row->nsid,
            'email' => check_plain($row->email),
            'info' => $info,
            'list_title' => $lists,
            'created' => format_date($row->created, 'short'),
            'confirmed' => $row->confirmed ? t('Yes, on') . '<br/>' . format_date($row->confirmation_timestamp, 'short') : t('No'),
            'operations' => array(
                'data' => array(
                    array(
                        '#type' => 'link',
                        '#title' => t('edit'),
                        '#href' => 'admin/config/media/newsletter/subscribers/edit/' . $row->nsid,
                        '#options' => array('query' => $destination),
                    ),
                    array(
                        '#markup' => ' | '
                    ),
                    array(
                        '#type' => 'link',
                        '#title' => t('delete'),
                        '#href' => 'admin/config/media/newsletter/subscribers/delete/' . $row->nsid,
                        '#options' => array('query' => $destination),
                    ),
                ),
            ),
        );
    }

    $form['subscriber'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $options,
        '#empty' => t('No subscribers yet.'),
    );

    $form['pager'] = array('#theme' => 'pager');
    return $form;
}